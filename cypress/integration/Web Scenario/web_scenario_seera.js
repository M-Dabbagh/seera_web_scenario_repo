
describe('SEERA - Web Scenario', () => {

    var base_url = 'https://www.tajawal.com/en/flights/originAirport-destinationAirport/departDate/returnDate/Economy/1Adult';

   // beforeEach(() => {
     // Navigating to tajwal URL
      //cy.visit(base_url)
    //})

    it('Selecting airports and date', () => {
        
        cy.visit(base_url)

        // Selecting Origin Airport
        cy.get('[data-testid="FlightSearchBox__FromAirportInput"]').type("Dubai International Airport")
           
        if (cy.get('ul[data-testid="AutoCompleteResultsList"] li[data-testid="FlightSearchBox__AirportOption1"] span.sc-hdNmWC').should('have.text', 'DXB')) {
            cy.get('ul[data-testid="AutoCompleteResultsList"] li[data-testid="FlightSearchBox__AirportOption1"]').click()
        }

        // Selecting Destination Airport
        cy.get('[data-testid="FlightSearchBox__ToAirportInput"]').type('Queen Alia international Airport')

        if (cy.get('ul[data-testid="AutoCompleteResultsList"] li[data-testid="FlightSearchBox__AirportOption0"] span.sc-hdNmWC').should('have.text', 'AMM')) {
            cy.get('ul[data-testid="AutoCompleteResultsList"] li[data-testid="FlightSearchBox__AirportOption0"]').click()
        }
        
        // Chossing Departure Date (Date From)
        cy.get('[data-testid="FlightSearchBox__FromDateButton"]').click()

        var today_date
        var today_date_value

        cy.get('.DayPicker-Day--today').then(message => {
            let today_date = message;
            cy.wrap(today_date).as('today_date')
        });

        cy.get('@today_date').then(today_date => {
            cy.log('today_date is === '+today_date.text())
            const txt = today_date.text()
            var today_date_as_int = parseInt(txt)
            
            if(today_date_as_int == 27 || today_date_as_int == 28 || today_date_as_int == 29 || today_date_as_int == 30){
    
                today_date_as_int = 3
                cy.get('[data-testid="FlightSearchCalendar__NextMonthButton"]').click()
            }else{
                today_date_as_int = today_date_as_int + 1
            }
            
            
    cy.log('today_date_as_int after if statement is === '+ today_date_as_int)

  
    var min = today_date_as_int
    var max = 28
    var date = Math.floor(Math.random() * (max - min + 1) ) + min;
    
    cy.log('date value is ==='+date)

    cy.xpath('//div[@class="DayPicker-Body"]//span[text()="'+date+'"]').eq(0).click()

    if (date <= 9){
        date = '0'+date
    }

    cy.xpath("//div[@data-testid='FlightSearchBox__FromDateButton']//span[@class='sc-hrBRpH gdnXTI']").should('have.text', date)
  });

  
  // Choosing Returning Date (Date To)
  cy.get('[data-testid="FlightSearchBox__ToDateButton"]').click()

  var date_return = Math.floor(Math.random() * (28 - 1 + 1) ) + 1;

  cy.xpath('//div[@class="DayPicker-Body"]//span[text()="'+date_return+'"]').eq(1).click()
  
  if(date_return <= 9){
      date_return = '0'+date_return
  }

  cy.xpath("//div[@data-testid='FlightSearchBox__ToDateButton']//span[@class='sc-hrBRpH gdnXTI']").should('have.text', date_return)

  // Clicking on Search button to dislay the results
  cy.xpath("//div[@class='d-none d-md-block sc-kBzFSH fyLibR']//button[@data-testid='FlightSearchBox__SearchButton']//span[text()='Search flights']").click()  
  
      })



      it('Comparing the cheapest flight with min price', () => {
       
        // Checking that results loaded on the page by using 2 assersions for that as the follwoing 2 lines
        cy.get('button[data-testid="Cheapest__SortBy"]', { timeout: 10000 }).should('be.visible');  
       
        cy.xpath('//div[contains(text(),"Getting the best deals from over")]', { timeout: 10000 }).should('not.be.visible');

        // Applying "cheapest" sorting feature on flight listing page
        cy.get('button[data-testid="Cheapest__SortBy"]').then(message => {
            let cheapestBtn = message;
            cy.wrap(cheapestBtn).as('cheapestBtn')
        });

        cy.get('@cheapestBtn').then(cheapestBtn => {
            cy.log('cheapestBtn is === '+cheapestBtn.text())
            const txt = cheapestBtn.text()
            var cheapestBtn_number_value = txt.split('AED')[1].replace(',', '').trim();
            cy.log('cheapestBtn_number_value is =='+cheapestBtn_number_value)

            var cheapestBtn_as_number = Number(cheapestBtn_number_value);
            cy.log('cheapestBtn_as_number is =='+cheapestBtn_as_number) 

            cy.get('button[data-testid="Cheapest__SortBy"]').click()
            
            // getting the price label value from the cheapest flight after sorting
            cy.get('[data-testid="Group0__PriceLabel"]').then(message => {
                let cheapestPriceLabel= message;
                cy.wrap(cheapestPriceLabel).as('cheapestPriceLabel')
            }); 

            cy.get('@cheapestPriceLabel').then(cheapestPriceLabel => {
                const txt_price_label = cheapestPriceLabel.text()
                var cheapestPriceLabel_number_value = txt_price_label.replace(',', '').trim();
                cy.log('cheapestPriceLabel_number_value is =='+cheapestPriceLabel_number_value)

                var cheapestPriceLabel_as_number = Number(cheapestPriceLabel_number_value);
                cy.log('cheapestPriceLabel_as_number is =='+cheapestPriceLabel_as_number)
                
                // clicking on Price filter to expand it and get the value of min
                cy.xpath('//div//span[text()="Price"]').click()

                cy.get('span[data-testid="FlightSearchResult__PriceFilter__Min"]').should('be.visible')

                // getting the value of min price on the price bar
                cy.get('span[data-testid="FlightSearchResult__PriceFilter__Min"]').then(message => {
                    let min_Price= message;
                    cy.wrap(min_Price).as('min_Price')
                }); 

                //
                cy.get('@min_Price').then(min_Price => {
                    const txt_min_Price = min_Price.text()
                    var min_Price_number_value = txt_min_Price.split('AED')[1].replace(',', '').trim();
                    cy.log('min_Price_number_value is =='+min_Price_number_value)
    
                    var min_Price_as_number = Number(min_Price_number_value);
                    cy.log('min_Price_as_number is =='+min_Price_as_number)

                    //assert(min_Price_as_number == cheapestBtn_as_number) and (cheapestBtn_as_number == cheapestPriceLabel_as_number))

                    var cheapest_values = false
                    if ((min_Price_as_number == cheapestPriceLabel_as_number) & (cheapestPriceLabel_as_number == cheapestBtn_as_number)){
                        cheapest_values = true
                    }
                    expect(cheapest_values).to.be.true
                    
                });
            });
        });

        
       

      })


})